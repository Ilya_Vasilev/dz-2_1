package com;

import java.util.Scanner;

public class Task2_2 {
    public static void main(String[] args) {
        final int MAX = 1000;
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        for (int i = 0; i < n; i++) {
            arr[i] = (int) Math.pow(arr[i], 2);
        }

        int[] count = new int[MAX];
        for (int i = 0; i < n; i++) {
            count[arr[i]]++;
        }
        int arrIndex = 0;
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                arr[arrIndex] = i;
                arrIndex++;
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
