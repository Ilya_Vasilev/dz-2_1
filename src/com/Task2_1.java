package com;

import java.util.Random;
import java.util.Scanner;

public class Task2_1 {
    public static void main(String[] args) {

        instruction();
        password();
    }

    public static void password() {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину желаемого пароля: ");
        int n = in.nextInt();
        while (n < 8) {
            System.out.println("Пароль c " + n + " количеством символов небезопасен. ");
            System.out.print("Введите еще раз: ");
            n = in.nextInt();
        }
        passwordCheck(n);
    }

    public static void passwordCheck(int n) {
        Random rand = new Random();
        String lower = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_*-";
        boolean isUpper = false, isLower = false, isNumber = false, isSymbol = false;

        String[] str = new String[n];
        for (int i = 0; i < n; i++) {
            str[i] = String.valueOf(lower.charAt(rand.nextInt(lower.length())));
        }
        for (int i = 0; i < n; i++) {
            if (str[i].matches(".*[A-Z].*")) {
                isUpper = true;
            } else if (str[i].matches(".*[a-z].*")) {
                isLower = true;
            } else if (str[i].matches(".*[0-9].*")) {
                isNumber = true;
            } else if (str[i].matches(".*[_*-].*")) {
                isSymbol = true;
            }
        }

        for (int i = 0; i < n; i++) {
            if (isUpper && isLower && isNumber && isSymbol) {
                System.out.print(str[i]);
            }
        }
        if (!isUpper || !isSymbol || !isNumber || !isLower) {
            passwordCheck(n);
        }
    }

    public static void instruction() {
        /**
         * Инструкция
         */
        System.out.println("Требования к паролю: " +
                "\n" + " * заглавные латинские символы " + "\n" +
                " * строчные латиские символы " + "\n" + " * числа" + "\n" + " * специальные символы");
    }
}



