package com;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        instruction();
        game();
    }

    public static void game() {
        int rand_num = (int) (Math.random() * 1001);

        Scanner in = new Scanner(System.in);
        System.out.print("Введите число от 0 до 1000: ");
        int a = in.nextInt();

        while (true) {
            if (a > rand_num) {
                System.out.println("Искомое число меньше загаданного.");
                a = in.nextInt();
            } else if (a == rand_num) {
                System.out.println("Победа!");
                break;
            } else {
                System.out.println("Искомое число больше загаданного.");
                a = in.nextInt();
            }
            if (a < 0) {
                break;
            }
        }
    }

    /**
     * Инструкция
     */
    public static void instruction() {
        System.out.println("Случайным образом в диапазоне от 0 до 1000 компьютер генерирует число." +
                " Вам необходимо разгадать это число. ");
    }
}


